<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use ChrisReedIO\Socialment\Facades\Socialment;
use ChrisReedIO\Socialment\Models\ConnectedAccount;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
    // Post Login Hook
    Socialment::preLogin(function (ConnectedAccount $connectedAccount) {
        dd($connectedAccount,'a');
    });
    // Multiple hooks can be added
    Socialment::preLogin(function (ConnectedAccount $connectedAccount) {
        // Handle additional custom pre login logic here if you need.
        dd($connectedAccount,'b');
    });

    // Post Login Hook
    Socialment::postLogin(function (ConnectedAccount $connectedAccount) {
        dd($connectedAccount,'c');
        Log::info('User logged in with ' . $connectedAccount->provider . ' account', [
            'connectedAccount' => $connectedAccount,
        ]);
    });
    }
}
