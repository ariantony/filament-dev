<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ConnectedAccount extends Model
{
    use HasFactory;

    protected $table = 'connected_accounts';

    protected $fillable = [
        'user_id',
        'provider',
        'provider_user_id',
        'name',
        'nickname',
        'email',
        'phone',
        'avatar',
        'token',
        'refresh_token',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}


