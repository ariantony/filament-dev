<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ConnectedAccount;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    //
 public function googleCallback() {
    $googleUser = Socialite::driver('google')->user();

    $existingUser = User::where('email', $googleUser->email)->first();

    if ($existingUser) {
        // Log in the existing user
        Auth::login($existingUser, true);
    } else {
        // Create a new user
        $newUser = User::create([
            'name' => $googleUser->name,
            'email' => $googleUser->email,
            'provider' => 'google',
            'provider_user_id' => $googleUser->id,
            'avatar' => $googleUser->avatar,
        ]);

        // Log in the new user
        Auth::login($newUser, true);
    }

    // Redirect to your desired page after authentication
    return redirect()->route('filament.admin.pages.dashboard');
    }

    public function facebookCallback() {
        $facebookUser = Socialite::driver('facebook')->user();
        $existingUser = User::where('provider_user_id', $facebookUser->id)->first();

        if ($existingUser) {
            // Log in the existing user
            Auth::login($existingUser, true);
        } else {
            // Create a new user
            $newUser = User::create([
                'name' => $facebookUser->name,
                'email' => $facebookUser->id . '@fb.com',
                'provider' => 'fb',
                'provider_user_id' => $facebookUser->id,
                'avatar' => $facebookUser->avatar,
            ]);

            // Log in the new user
            Auth::login($newUser, true);
        }

        // Redirect to your desired page after authentication
        return redirect()->route('filament.admin.pages.dashboard');
        }
 }

