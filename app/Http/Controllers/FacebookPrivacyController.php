<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FacebookPrivacyController extends Controller
{
    public function home()
    {
        return view('static.privacy'); // 'static.home' is the blade view file for the home page
    }
}
