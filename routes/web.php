<?php

use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\FacebookPrivacyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/google/redirect',[SocialController::class,'googleCallback'])->name('google.callback');
Route::get('/facebook/redirect',[SocialController::class,'facebookCallback'])->name('facebook.callback');
Route::get('/facebook/privacy_policy',[FacebookPrivacyController::class,'home'])->name('facebook.privacy');
